# Hokuyo MAUVE package

This package defines the MAUVE component for the Hokuyo driver.

This package is under GPL License.

## Installation instructions

```bash
mkdir -p ~/mauve_ws/src
cd ~/mauve_ws
git clone https://gitlab.com/MAUVE/packages/hokuyo_mauve.git src/hokuyo_mauve
catkin_make
source devel/setup.bash
```

## Documentation

### Output Ports

* *scan* (LaserScan): measured laser scan

### Properties

#### Shell Properties

* *device* (string): device name (default "/dev/ttyACM0")
* *baudrate* (unsigned int): connection baud rate (default 115200)
* *first_angle* (double): first angle of the scan span (in radians, default -1.57 $\simeq -\frac{\pi}{2}$)
* *last_angle* (double): last angle of the scan span (in radians, default 1.57 $\simeq \frac{\pi}{2}$)
* *angle_resolution* (double): angular resolution of the scan (in radians, default 0 -- get all rays)

#### FSM Properties

* *period* (double): period of the state machine (in ns, default 1s)

### State Machine

<p>
  <img align="center" src="doc/Hokuyo.png"/>
</p>

* *E*: read scan from device and write it

## Testing

To test the connection with the laser:
```
rosrun hokuyo_mauve hokuyo_mauve_component
```
