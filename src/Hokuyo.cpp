/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Hokuyo project.
 *
 * MAUVE Hokuyo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Hokuyo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#include <urg_c/urg_sensor.h>
#include <urg_c/urg_serial.h>
#include <urg_c/urg_utils.h>

#include "hokuyo/mauve/Hokuyo.hpp"

namespace hokuyo {
namespace mauve {

bool HokuyoShell::configure_hook() {
  if (first_angle > last_angle) { // revert
    double temp = last_angle;
    last_angle = first_angle;
    first_angle = temp;
  }
  if (last_angle - first_angle < angle_resolution) {
    logger().warn("scan span looks too small: ({}, {})", first_angle, last_angle);
    return false;
  }
  return true;
}

bool HokuyoCore::configure_hook() {
  int min_step, max_step;
  long int min_distance, max_distance;
  long int range_min, range_max;
  // Open
  int r = urg_open(&fd, URG_SERIAL,
    shell().device.get_value().c_str(), shell().baudrate.get());
	if (r == 0) {
    logger().debug("status: {}", urg_sensor_status(&fd));
    logger().debug("product type: {}", urg_sensor_product_type(&fd));
    logger().debug("firmware version: {}", urg_sensor_firmware_version(&fd));
	  logger().debug("serial ID: {}", urg_sensor_serial_id(&fd));
    urg_step_min_max(&fd, &min_step, &max_step);
	  logger().debug("step: [{}, {}]", min_step, max_step);
    urg_distance_min_max(&fd, &min_distance, &max_distance);
    logger().debug("distance: [{}, {})", min_distance, max_distance);
	  logger().debug("scan interval: {}", urg_scan_usec(&fd));
	  logger().debug("sensor data size: {}", urg_max_data_size(&fd));
	}
  else {
    logger().error("error on urg_open {} {}: {}",
      shell().device.get_value(), shell().baudrate.get(),
      urg_error(&fd));
    return false;
  }
  // Set parameters
  int first_step = urg_rad2step(&fd, shell().first_angle);
	int last_step = urg_rad2step(&fd, shell().last_angle);
	int res_steps = ceil((shell().last_angle - shell().first_angle) / shell().angle_resolution);
	int skip_step = (shell().angle_resolution > 0) ? (last_step - first_step) / res_steps : 1;
	int e = urg_set_scanning_parameter(&fd, first_step, last_step, skip_step);
	if (e == 0) {
		scan_size = ceil((last_step - first_step + 1) * 1.0 /skip_step);
	}
	else {
		logger().error(urg_error(&fd));
		return false;
	}
  // Init scan structure
  scan.angle_min = urg_step2rad(&fd, fd.scanning_first_step);
  scan.angle_max = urg_step2rad(&fd, fd.scanning_last_step);

  int step_range = max_step - min_step;
  double angle_range = urg_step2rad(&fd, max_step) - urg_step2rad(&fd, min_step);
  scan.angle_increment = fd.scanning_skip_step * (angle_range / step_range);

  urg_distance_min_max(&fd, &range_min, &range_max);
  scan.range_min = range_min * 0.001;
  scan.range_max = range_max * 0.001;
  scan.ranges.resize(scan_size);
  int data_size = urg_max_data_size(&fd);
  data = (long*) malloc(data_size * sizeof(long));
  return true;
}

void HokuyoCore::cleanup_hook() {
  urg_close(&fd);
  free(data);
}

bool HokuyoCore::start_measurement(int scan_times) {
  int result;
  result = urg_start_measurement(&fd, URG_DISTANCE, scan_times, 0);
  if (result < 0) {
    logger().error("Error starting measurements {}", urg_error(&fd));
    return false;
  }
  logger().debug("measurement started ({})", scan_times);
  return true;
}

int HokuyoCore::get_measurement() {
  int result = urg_get_distance(&fd, data, &timestamp, nullptr);
  if (result <= 0) {
    logger().warn(urg_error(&fd));
    return result;
  }
  logger().debug("grabbed scan of size {} at {}", result, timestamp);

  int f = urg_step2index(&fd, fd.scanning_first_step);
  int l = urg_step2index(&fd, fd.scanning_last_step);
  scan.ranges = std::vector<double>(data + f, data + l);
  std::for_each(scan.ranges.begin(), scan.ranges.end(),
    [](double& v) { v *= 0.001; } );
  return result;
}

bool HokuyoCore::stop_measurement() {
  if (urg_stop_measurement(&fd) < 0) {
    logger().warn(urg_error(&fd));
    return false;
  }
  logger().info("measurement stopped");
  return true;
}

void HokuyoCore::update() {
  int s = get_measurement();
  if (s > 0) {
    shell().scan.write(scan);
    shell().connection_error.write(false);
  } else
    shell().connection_error.write(true);
  shell().size_error.write(s != scan_size);
}

bool HokuyoFSM::configure_hook() {
  if (! ::mauve::runtime::PeriodicStateMachine<HokuyoShell,HokuyoCore>::configure_hook())
    return false;
  core().start_measurement(0);
  return true;
}

void HokuyoFSM::cleanup_hook() {
  core().stop_measurement();
}

}}
