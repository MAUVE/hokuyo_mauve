/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Hokuyo project.
 *
 * MAUVE Hokuyo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Hokuyo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/gpl-3.0>.
 */
#ifndef MAUVE_HOKUYO_HOKUYO_HPP
#define MAUVE_HOKUYO_HOKUYO_HPP

#include <string>
#include <urg_c/urg_sensor.h>

#include <mauve/runtime.hpp>
#include <mauve/types/sensor_types.hpp>

namespace hokuyo {
  namespace mauve {

  /** Shell of the Hokuyo driver. */
  struct HokuyoShell : public ::mauve::runtime::Shell {
    /** Device name. */
    ::mauve::runtime::Property<std::string>& device = mk_property<std::string>("device", "/dev/ttyACM0");
    /** Connection baud rate. */
    ::mauve::runtime::Property<unsigned int>& baudrate = mk_property("baudrate",
      static_cast<unsigned int>(115200));
    /** First angle of the scan span. */
		::mauve::runtime::Property<double>& first_angle = mk_property("first_angle", -1.57);
    /** Last angle of the scan span. */
    ::mauve::runtime::Property<double>& last_angle = mk_property("last_angle", 1.57);
    /** Angular resolution of the scan. */
	  ::mauve::runtime::Property<double>& angle_resolution = mk_property("angle_resolution", 0.0);
    /** Output port with the scan. */
    ::mauve::runtime::WritePort<::mauve::types::sensor::LaserScan>& scan = mk_write_port<::mauve::types::sensor::LaserScan>("scan");
    /** Output port for connection error status. */
    ::mauve::runtime::WritePort<bool>& connection_error = mk_write_port<bool>("connection_error");
    /** Output port for scan size error status. */
    ::mauve::runtime::WritePort<bool>& size_error = mk_write_port<bool>("size_error");

    virtual bool configure_hook() override;
  }; // HokuyoShell

  /** Core of the Hokuyo driver. */
  struct HokuyoCore : public ::mauve::runtime::Core<HokuyoShell> {
    /** File descriptor for the connection to the sensor. */
    urg_t fd;
    /** The scan structure. */
    ::mauve::types::sensor::LaserScan scan;
    /** Scan size. */
    size_t scan_size;
    /** Buffer to store received data. */
    long* data;
    /** Timestamp of acquired scan. */
    long timestamp;

    virtual bool configure_hook() override;
    virtual void cleanup_hook() override;

    bool start_measurement(int scan_times);
    int get_measurement();
    bool stop_measurement();

    void update();
  };

  /** Periodic State-Machine of the Hokuyo driver. */
  struct HokuyoFSM : public ::mauve::runtime::PeriodicStateMachine<HokuyoShell, HokuyoCore> {
  protected:
    virtual bool configure_hook() override;
    virtual void cleanup_hook() override;
  };

  /** Definition of an Hokuyo component. */
  using Hokuyo = ::mauve::runtime::Component<HokuyoShell, HokuyoCore, HokuyoFSM>;

}} // namespace

#endif
